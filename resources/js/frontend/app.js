require('./global');
require('./management-club');
require('./club');
require('./tournament');

/* default namespace for view variables and functions */
window.league = window.league || {};

/* preview image when upload */
league.previewImage = (input, output) => {
  if (input.files && input.files[0]) {
    let reader = new FileReader();

    reader.onload = function (e) {
      output.attr('src', e.target.result).removeClass('d-none');
    };

    reader.readAsDataURL(input.files[0]);
  }
};
