$(document).ready(function() {
  // Preview image at create/edit item
  const ITEM_IMG_ELM = $('.create-club');

  ITEM_IMG_ELM.on('change', '#upload-logo', function (e) {
    league.previewImage(this, $(this).parents('.input-group').parent().find('.icon-logo'));
  });

  ITEM_IMG_ELM.on('change', '#uploadUniform', function (e) {
    league.previewImage(this, $(this).parents('.input-group').parent().find('.icon-uniform'));
  });
});