$(document).ready(function() {
  $('#open-detail').click(function () {
    $('.detail-tournaments-joined').slideDown(300);
    $('#close-detail').removeClass('d-none');
    $(this).addClass('d-none');
  });

  $('#close-detail').click(function () {
    $('.detail-tournaments-joined').slideUp(300);
    $('#open-detail').removeClass('d-none');
    $(this).addClass('d-none');
  });

  $('#open-detail-join').click(function () {
    $('.detail-tournaments-join').slideDown(300);
    $('#close-detail-join').removeClass('d-none');
    $(this).addClass('d-none');
  });

  $('#close-detail-join').click(function () {
    $('.detail-tournaments-join').slideUp(300);
    $('#open-detail-join').removeClass('d-none');
    $(this).addClass('d-none');
  });

  $('#open-detail-registered').click(function () {
    $('.detail-tournaments-registered').slideDown(300);
    $('#close-detail-registered').removeClass('d-none');
    $(this).addClass('d-none');
  });

  $('#close-detail-registered').click(function () {
    $('.detail-tournaments-registered').slideUp(300);
    $('#open-detail-registered').removeClass('d-none');
    $(this).addClass('d-none');
  });
});
