$(document).ready(function() {
  $('#form-competition-1').click(function (e) {
    $(this).css('border', '2px solid #3bbf1a');
    $('#form-competition-2, #form-competition-3').css('border', 'none');
    $('.alert-tournament').css('display', 'block');
    $('.content-form-competition-2, .content-form-competition-3').css('display', 'none');
  });

  $('#form-competition-2').click(function (e) {
    $(this).css('border', '2px solid #3bbf1a');
    $('#form-competition-1, #form-competition-3').css('border', 'none');
    $('.content-form-competition-2').css('display', 'block');
    $('.content-form-competition-3').css('display', 'none');
  });
  $('#form-competition-3').click(function (e) {
    $(this).css('border', '2px solid #3bbf1a');
    $('#form-competition-1, #form-competition-2').css('border', 'none');
    $('.content-form-competition-3').css('display', 'block');
    $('.content-form-competition-2').css('display', 'none');
  });

  // Preview image at create/edit item
  const ITEM_IMG_ELM = $('.create-tournament');

  ITEM_IMG_ELM.on('change', '#upload-banner', function (e) {
    league.previewImage(this, $(this).parents('.col-sm-5').find('.icon-banner'));
  });

});