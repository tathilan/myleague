<!doctype html>
<html lang="vn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>MY LEAGUE</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link href="" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ mix('frontend/css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('frontend/css/vendor.css') }}" rel="stylesheet">
</head>
<body>
<div class="c-wrapper c-fixed-components">
    <div class="fade-in">
        <div class="main-login" style="background: url('{{ asset('image/background-login.jpg')}}') no-repeat; background-size: cover;">
            <div class="col-4">
                <div class="form-login">
                    <form method="POST" action="{{route('user.login')}}">
                        @csrf
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-success">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!--fade-in-->
</div><!--c-wrapper-->
@stack('before-scripts')
<script src="{{ mix('frontend/js/vendor.js') }}"></script>
<script src="{{ mix('frontend/js/app.js') }}"></script>
<livewire:scripts/>
@stack('after-scripts')
</body>
</html>
