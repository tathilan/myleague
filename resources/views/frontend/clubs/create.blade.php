@extends('frontend.layouts.app')

@section('content')
    <div class="create-club mb-3">
        <div class="container-fluid d-flex align-items-center justify-content-center">
            <div class="col-sm-9">
                <div class="card">
                    <div class="card-header">
                        Tạo đội
                        <div class="message-require">Vui lòng nhập thông tin hợp lệ cho các trường được yêu cầu <span class="text-red">*</span></div>
                    </div>
                    <form accept-charset="UTF-8" action="{{route('user.clubs.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">

                            <div class="frame-register">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <label for="exampleInputEmail1">Ảnh đại diện</label>
                                        <div class="logo-club">
                                            <img class="icon-logo" width="242" height="242" src="{{asset('image/team.png')}}">
                                        </div>

                                        <div class="input-group mt-3 mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="upload-logo" name="avatar">
                                                <label class="custom-file-label" for="logo">Logo đội của bạn</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-7">
                                        <form>
                                            <div class="form-group">
                                                <label for="name">Tên đội<span class="text-red">*</span></label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Tên đội của bạn">
                                            </div>
                                            <div class="form-group">
                                                <label for="level">Trình độ<span class="text-red">*</span></label>
                                                <select class="form-control" id="level" name="level">
                                                    <option value="">Vui lòng chọn</option>
                                                    <option value="4">Chuyên nghiệp</option>
                                                    <option value="3">Bán chuyên nghiệp</option>
                                                    <option value="2">Trung cấp</option>
                                                    <option value="1">Vui khoẻ</option>
                                                    <option value="0">Khác</option>
                                                </select>
                                            </div>
                                            <div class="form-group-inline">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="form-group">
                                                            <label for="telephone_number">Số điện thoại<span class="text-red">*</span></label>
                                                            <input type="text" name="telephone_number" class="form-control" id="telephone_number" placeholder="Số điện thoại">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="status">Chế độ</label>
                                                            <select class="form-control" id="status" name="status">
                                                                <option value="1">Công khai</option>
                                                                <option value="0">Bí mật</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="gender">Giới tính<span class="text-red">*</span></label>
                                                <select class="form-control" id="gender" name="gender">
                                                    <option value="">Vui lòng chọn</option>
                                                    <option value="0">Nam</option>
                                                    <option value="1">Nữ</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="frame-register">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="email">Địa chỉ email</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="name_contact">Tên người liên hệ</label>
                                            <input type="text" name="people_contact" class="form-control" id="people_contact" placeholder="Tên người liên hệ">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="age">Độ tuổi</label>
                                            <select class="form-control" id="age" name="age">
                                                <option value="">Vui lòng chọn</option>
                                                <option value="0">15-20</option>
                                                <option value="1">20-25</option>
                                                <option value="2">25-30</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group-inline col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="area">Khu vực hoạt động</label>
                                                    <input type="text" class="form-control" id="area" name="area" placeholder="Nhập vị trí">
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label for="time_active">Khung thời gian hoạt động</label>
                                                    <input type="text" class="form-control" id="time_active" name="time_activity" placeholder="VD: T2-07:23 hoặc CN-18:23">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="frame-register">
                                <div class="row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-4">
                                        <label for="exampleFormControlSelect1">Đồng phục</label>
                                        <div class="logo-club mb-3">
                                            <img class="icon-uniform" width="242" height="242" src="{{asset('image/shirt.svg')}}">
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input name="uniform" type="file" class="custom-file-input" id="uploadUniform">
                                                <label class="custom-file-label" for="uploadUniform">Đồng phục đội</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4"></div>
                                </div>
                            </div>

                            <div class="last-register">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="introduce">Giới thiệu về đội </label>
                                            <textarea name="introduce" class="form-control" id="introduce" placeholder="Lời giới thiệu về đội"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="frame-register text-center border-bottom-0">
                                <button type="submit" class="btn btn-success">Lưu</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
