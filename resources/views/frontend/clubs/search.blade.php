@extends('frontend.layouts.app')

@section('content')
    <div class="search-club mb-3">
        <div class="container-fluid">
            <div class="row form-search">
                <div class="col-sm-5">
                    <div class="input-group">
                        <input class="form-control" placeholder="Tên giải đấu, tên người quản lý" name="keyword" type="text">
                        <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                    </div>
                </div>

                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option>Trình độ</option>
                                    <option>Chuyên nghiệp</option>
                                    <option>Bán chuyên nghiệp</option>
                                    <option>Trung cấp</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option>Giới tính</option>
                                    <option>Nam</option>
                                    <option>Nữ</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row result-search-club">
                <div class="col-sm-12 col-md-3 mb-3">
                    <div class="club">
                        <div class="avatar-club mb-2">
                            <a href="#">
                                <img class="w-100" src="{{asset('image/team.png')}}" alt="avatar-club">
                            </a>
                        </div>
                        <a href="#" class="name-club">GBG</a>
                        <div>Trinh do || Gioi tinh</div>
                        <div class="member-club">
                            <div>Thanh vien</div>
                            <div class="flex-item">
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-3 mb-3">
                    <div class="club">
                        <div class="avatar-club mb-2">
                            <a href="#">
                                <img class="w-100" src="{{asset('image/team.png')}}" alt="avatar-club">
                            </a>
                        </div>
                        <a href="#" class="name-club">GBG</a>
                        <div>Trinh do || Gioi tinh</div>
                        <div class="member-club">
                            <div>Thanh vien</div>
                            <div class="flex-item">
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 mb-3">
                    <div class="club">
                        <div class="avatar-club mb-2">
                            <a href="#">
                                <img class="w-100" src="{{asset('image/team.png')}}" alt="avatar-club">
                            </a>
                        </div>
                        <a href="#" class="name-club">GBG</a>
                        <div>Trinh do || Gioi tinh</div>
                        <div class="member-club">
                            <div>Thanh vien</div>
                            <div class="flex-item">
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 mb-3">
                    <div class="club">
                        <div class="avatar-club mb-2">
                            <a href="#">
                                <img class="w-100" src="{{asset('image/team.png')}}" alt="avatar-club">
                            </a>
                        </div>
                        <a href="#" class="name-club">GBG</a>
                        <div>Trinh do || Gioi tinh</div>
                        <div class="member-club">
                            <div>Thanh vien</div>
                            <div class="flex-item">
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                                <div class="member">
                                    <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
