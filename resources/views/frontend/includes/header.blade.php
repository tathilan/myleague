<div class="header-base">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="navbrand">
            <h1><a href="./index.html" class="brand">LEAGUE</a></h1>
            <div class="burger" id="burger">
			<span class="burger-open">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="16">
					<g fill="#fff" fill-rule="evenodd">
						<path d="M0 0h24v2H0zM0 7h24v2H0zM0 14h24v2H0z"/>
					</g>
				</svg>
			</span>
                <span class="burger-close">
				<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20">
					<path fill="#fff" fill-rule="evenodd"
                          d="M17.778.808l1.414 1.414L11.414 10l7.778 7.778-1.414 1.414L10 11.414l-7.778 7.778-1.414-1.414L8.586 10 .808 2.222 2.222.808 10 8.586 17.778.808z"/>
				</svg>
			</span>
            </div>
        </div>
        <ul class="menu" id="menu">
            <li class="menu-item"><a href="{{route('home')}}" class="menu-link">Trang chủ</a></li>
            <li class="menu-item"><a href="{{route('clubs.showFormSearch')}}" class="menu-link">Tìm đội</a></li>
            <li class="menu-item"><a href="{{route('tournaments.showFormSearch')}}" class="menu-link">Tìm giải đấu</a>
            </li>

            @if (Auth::check())
                <li class="menu-item"><a href="{{route('user.tournaments.showFormCreate')}}" class="menu-link">Tạo giải
                        đấu</a></li>
                <li class="menu-item"><a href="{{route('user.clubs.showFormCreate')}}" class="menu-link">Tạo đội</a></li>
                <li class="dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                        Welcome, User <b class="caret"></b>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right position-absolute" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            @else
                <li class="menu-item"><a href="{{route('user.showLoginForm')}}" class="menu-link">Đăng nhập</a></li>
                <li class="menu-item"><a href="#" class="menu-link">Đăng ký</a></li>
            @endif
        </ul>
    </nav>
</div>
