@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="management-club">
            @include('frontend.users.includes.sidebar')
            <div class="c-wrapper c-fixed-components">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="card">
                            <div class="card-header text-center">
                                Thong ke thanh tich

                            </div><!--card-header-->

                            <div class="card-body">
                                <div class="row achievement-statistics">
                                    <div class="col-12 text-center index-statistics">
                                        <div class="title">
                                            <span>Giai thuong</span>
                                        </div>
                                        <div class="content">
                                            <div class="row">
                                                <div class="index col-md-3">
                                                    <div class="solution">Vo dich</div>
                                                    <img src="{{asset('image/champion.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-3">
                                                    <div class="solution">Giai nhi</div>
                                                    <img src="{{asset('image/silver-medal.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-3">
                                                    <div class="solution">Giai ba</div>
                                                    <img src="{{asset('image/bronze-medal.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-3">
                                                    <div class="solution">Giai tu</div>
                                                    <img src="{{asset('image/flag.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center index-statistics mt-5">
                                        <div class="title">
                                            <span>Tran dau</span>
                                        </div>
                                        <div class="content">
                                            <div class="row">
                                                <div class="index col-md-3">
                                                    <div class="solution">Tong so tran</div>
                                                    <img src="{{asset('image/match.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-3">
                                                    <div class="solution">So tran thang</div>
                                                    <img src="{{asset('image/seo-contest.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-3">
                                                    <div class="solution">So tran hoa</div>
                                                    <img src="{{asset('image/competition.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-3">
                                                    <div class="solution">So tran thua</div>
                                                    <img src="{{asset('image/man-losing-hat.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center index-statistics mt-5">
                                        <div class="title">
                                            <span>Ban thang thua</span>
                                        </div>
                                        <div class="content">
                                            <div class="row">
                                                <div class="index col-md-4">
                                                    <div class="solution">ban thang</div>
                                                    <img src="{{asset('image/goal.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-4">
                                                    <div class="solution">thung luoi</div>
                                                    <img src="{{asset('image/estadisticas_gol.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-4">
                                                    <div class="solution">phan luoi</div>
                                                    <img src="{{asset('image/estadisticas_gol.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center index-statistics mt-5">
                                        <div class="title">
                                            <span>The phat</span>
                                        </div>
                                        <div class="content">
                                            <div class="row">
                                                <div class="index col-md-6">
                                                    <div class="solution">The do</div>
                                                    <img src="{{asset('image/card-red.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>

                                                <div class="index col-md-6">
                                                    <div class="solution">The vang</div>
                                                    <img src="{{asset('image/card-yellow.png')}}" alt="champion" width="120" height="120">
                                                    <div class="number">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--card-body-->

                        </div><!--card-->
                    </div><!--fade-in-->
                </div><!--container-fluid-->
            </div>
        </div>
    </div>

@endsection
