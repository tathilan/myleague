@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="management-all">
            <div class="info-user mb-5">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <div class="avatar">
                            <img src="{{asset('image/team.png')}}">
                        </div>
                    </div>
                    <div class="col-8 col-md-4">
                       <div class="email-user mb-2">
                           <i class="fas fa-envelope"></i>
                            lanta12a5@gmail.com
                       </div>
                       <div class="phone-number-user mb-2">
                           <i class="fas fa-phone-alt"></i>
                           0343900246
                       </div>
                       <div class="time-activity">
                           <i class="fas fa-calendar-alt"></i>
                           20:30 T3 HMG
                       </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>

            </div>
            <div class="list-tab-management">
                <ul class="nav nav-tabs" role="tab-list">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">QUẢN LÝ GIẢI ĐẤU</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">QUẢN LÝ ĐỘI BÓNG</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">THỐNG KÊ</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                        <div class="container-fluid">
                            <div class="mb-4 mt-4">
                                <button class="btn btn-success">Tạo giải đấu</button>
                            </div>

                            <select class="form-control w-25 mb-4" id="status-tournament">
                                <option>Giải đấu đã tạo</option>
                                <option>Giải đấu đã tham hia</option>
                            </select>

                            <div class="list-tournament">
                                <label>Danh sách giải</label>

                                <table class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Tên giải</th>
                                        <th scope="col">Loại</th>
                                        <th scope="col">Người tạo</th>
                                        <th scope="col">Địa chỉ</th>
                                        <th scope="col">Trạng thái</th>
                                        <th scope="col">Số đội tham gia</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                        <div class="container-fluid">
                            <div class="btn-create mt-4 mb-4">
                                <button class="btn btn-success">Tạo đội</button>
                            </div>

                            <div class="list-club">
                                <div class="row result-search-club">
                                    <div class="col-sm-12 col-md-3 mb-3">
                                        <div class="club">
                                            <div class="avatar-club mb-2">
                                                <a href="#">
                                                    <img class="w-100" src="{{asset('image/team.png')}}" alt="avatar-club">
                                                </a>
                                            </div>
                                            <a href="#" class="name-club">GBG</a>
                                            <div>Trinh do || Gioi tinh</div>
                                            <div class="member-club">
                                                <div>Thanh vien</div>
                                                <div class="flex-item">
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-3 mb-3">
                                        <div class="club">
                                            <div class="avatar-club mb-2">
                                                <a href="#">
                                                    <img class="w-100" src="{{asset('image/team.png')}}" alt="avatar-club">
                                                </a>
                                            </div>
                                            <a href="#" class="name-club">GBG</a>
                                            <div>Trinh do || Gioi tinh</div>
                                            <div class="member-club">
                                                <div>Thanh vien</div>
                                                <div class="flex-item">
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3 mb-3">
                                        <div class="club">
                                            <div class="avatar-club mb-2">
                                                <a href="#">
                                                    <img class="w-100" src="{{asset('image/team.png')}}" alt="avatar-club">
                                                </a>
                                            </div>
                                            <a href="#" class="name-club">GBG</a>
                                            <div>Trinh do || Gioi tinh</div>
                                            <div class="member-club">
                                                <div>Thanh vien</div>
                                                <div class="flex-item">
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3 mb-3">
                                        <div class="club">
                                            <div class="avatar-club mb-2">
                                                <a href="#">
                                                    <img class="w-100" src="{{asset('image/team.png')}}" alt="avatar-club">
                                                </a>
                                            </div>
                                            <a href="#" class="name-club">GBG</a>
                                            <div>Trinh do || Gioi tinh</div>
                                            <div class="member-club">
                                                <div>Thanh vien</div>
                                                <div class="flex-item">
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                    <div class="member">
                                                        <img width="38" height="38" src="{{asset('image/team.png')}}" alt="avatar-member">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                       <div class="title mt-4 mb-4">
                           THỐNG KÊ
                       </div>

                        <div class="detail">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tên giải</th>
                                    <th scope="col">Loại</th>
                                    <th scope="col">Người tạo</th>
                                    <th scope="col">Địa chỉ</th>
                                    <th scope="col">Trạng thái</th>
                                    <th scope="col">Số đội tham gia</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
