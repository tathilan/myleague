@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="management-club">
            @include('frontend.users.includes.sidebar')
            <div class="c-wrapper c-fixed-components">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="card">
                            <div class="card-header text-center">
                                Cac giai dau
                            </div>

                            <div class="card-body">
                                <div class="row management-tournament">
                                    <div class="col-12">
                                        <div class="w-100 item-tournament-joined">
                                            <i class="fas fa-plus" id="open-detail"></i>
                                            <i class="fas fa-minus d-none    " id="close-detail"></i>
                                            Các giải đấu đã tham gia
                                        </div>
                                        <div class="form-group col-12 detail-tournaments-joined">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 tournament-registered">
                                                    <div class="background-tournament-default">
                                                        <img src="{{asset('image/bg_tournament.png')}}" alt="background">
                                                    </div>

                                                    <div class="info-tournament">
                                                        <div class="row">
                                                            <div class="col-5">
                                                                <div class="banner-tournament">
                                                                    <img src="{{asset('image/bg_tournament.png')}}" alt="banner">
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="name-tournament">GBG OPEN</div>
                                                                <span class="number-club-joined mr-2"><i class="fa fa-users"></i>8</span>
                                                                <span class="number-match-completed ml-2"><i class="fa fa-futbol"></i>4/10</span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="w-100 item-tournament-registered">
                                            <i class="fas fa-plus" id="open-detail-registered"></i>
                                            <i class="fas fa-minus d-none" id="close-detail-registered"></i>
                                            Các giải đấu đã tham gia
                                        </div>
                                        <div class="form-group col-12 detail-tournaments-registered">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 tournament-registered">
                                                    <div class="background-tournament-default">
                                                        <img src="{{asset('image/bg_tournament.png')}}" alt="background">
                                                    </div>

                                                    <div class="info-tournament">
                                                        <div class="row">
                                                            <div class="col-5">
                                                                <div class="banner-tournament">
                                                                    <img src="{{asset('image/bg_tournament.png')}}" alt="banner">
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="name-tournament">GBG OPEN</div>
                                                                <span class="number-club-joined mr-2"><i class="fa fa-users"></i>8</span>
                                                                <span class="number-match-completed ml-2"><i class="fa fa-futbol"></i>4/10</span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="w-100 item-tournament-join">
                                            <i class="fas fa-plus" id="open-detail-join"></i>
                                            <i class="fas fa-minus d-none" id="close-detail-join"></i>
                                            Các giải đấu đã tham gia
                                        </div>
                                        <div class="form-group col-12 detail-tournaments-join">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 tournament-registered">
                                                    <div class="background-tournament-default">
                                                        <img src="{{asset('image/bg_tournament.png')}}" alt="background">
                                                    </div>

                                                    <div class="info-tournament">
                                                        <div class="row">
                                                            <div class="col-5">
                                                                <div class="banner-tournament">
                                                                    <img src="{{asset('image/bg_tournament.png')}}" alt="banner">
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="name-tournament">GBG OPEN</div>
                                                                <span class="number-club-joined mr-2"><i class="fa fa-users"></i>8</span>
                                                                <span class="number-match-completed ml-2"><i class="fa fa-futbol"></i>4/10</span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!--card-body-->

                        </div><!--card-->
                    </div><!--fade-in-->
                </div><!--container-fluid-->
            </div>
        </div>
    </div>
@endsection
