@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="management-club">
            @include('frontend.users.includes.sidebar')
            <div class="c-wrapper c-fixed-components">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="card">
                            <div class="card-header text-center">
                                Thông tin thanh vien
                            </div>

                            <div class="card-body">
                                <button class="btn btn-success mb-3" data-toggle="modal" data-target="#createMemberClub"><i class="fas fa-plus"></i>Them thanh vien</button>
                                <div class="row">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="detail-member">
                                            <div class="mb-3 text-right">
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteMember"><i class="fas fa-trash-alt"></i></button>
                                            </div>

                                            <div class="role">Doi truong</div>
                                            <div class="d-flex align-items-center justify-content-center mt-3">
                                                <div class="avatar-member">
                                                    <img src="{{asset('image/team.png')}}">
                                                </div>
                                            </div>

                                            <div class="member-name">Ta Thi Lan</div>
                                            <div class="competitor-member-stats" style="">
                                                <ul>
                                                    <li class="hvr-icon-spin numberGoal" tabindex="0">
                                                        <span class="numMyLeague">0</span>
                                                        <span class="far fa-futbol hvr-icon" style="margin-left:0;margin-right:8px"></span>
                                                        | </li>
                                                    <li class="hvr-icon-grow numberLeague" tabindex="0">
                                                        <span class="totalTeamNumber">1</span>
                                                        <span class="fa fa-bookmark hvr-icon" style="margin-left:0;margin-right:8px"></span>
                                                    </li><br>
                                                    <li class="hvr-icon-grow hvr-icon-buzz-out numberCardYellow" tabindex="0">
                                                        <span class="totalAwardNumber">0</span>
                                                        <span class="card card--yellow hvr-icon"></span>
                                                        | </li>
                                                    <li class="hvr-icon-grow hvr-icon-buzz-out numberCardRed" tabindex="0">
                                                        <span class="totalAwardNumber">0</span> <span class="card card--red hvr-icon"></span>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="group-btn">
                                                <button class="btn btn-success" data-toggle="modal" data-target="#detailMemberClub"><i class="fas fa-file-alt"></i></button>
                                                <button class="btn btn-success" data-toggle="modal" data-target="#updateMemberClub"><i class="far fa-edit"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--card-body-->

                        </div><!--card-->
                    </div><!--fade-in-->
                </div><!--container-fluid-->
            </div>
        </div>
    </div>
    @include('frontend.users.includes.modal-edit-member')
    @include('frontend.users.includes.modal-delete-member')
    @include('frontend.users.includes.modal-detail-member')
    @include('frontend.users.includes.modal-create-member')
@endsection
