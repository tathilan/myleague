<div class="modal fade" id="detailMemberClub" tabindex="-1" role="dialog" aria-labelledby="detailMemberClub" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thông tin chi tiết</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <div id="editProfileMember" class="editProfileMember">
                        <form method="POST" class="form-update-player-member">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-5 col-sm-12 col-xs-12 personal-avatar">
                                        <div class="avatar-container d-flex align-items-center justify-content-center">
                                            <div class="avatar-position">
                                                <img src="{{asset('image/team.png')}}">
                                            </div>
                                        </div>
                                        <hr class="hidden-md hidden-lg">
                                        <div class="row d-flex align-items-center justify-content-center wrap-card">
                                            <label class="title"><b>Ảnh thẻ căn cước</b></label>
                                            <div class="avatar-card col-md-12 col-sm-12 col-xs-12 mb-5">
                                                <span class="title-card mb-3">Mặt trước:</span>
                                                <img src="{{asset('image/team.png')}}">
                                            </div>
                                            <div class="avatar-card col-md-12 col-sm-12 col-xs-12">
                                                <span class="title-card mb-3">Mặt sau:</span>
                                                <img src="{{asset('image/team.png')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- edit form column -->
                                    <div class="col-md-7 col-sm-12 col-xs-12 personal-info mt-15">
                                        <div class="form-group display-align">
                                            <label class="col-12 control-label required">Họ tên đầy đủ</label>
                                            <div class="col-lg-9 col-md-9 col-sm-8">
                                                <input class="form-control fullname" name="fullname" type="text" value="Lê Duy Thành">
                                            </div>
                                        </div>
                                        <div class="form-group display-align">
                                            <label class="col-12  control-label ">Tên thi đấu</label>
                                            <div class="col-lg-9 col-md-9 col-sm-8">
                                                <input class="form-control namePlayer" name="namePlayer" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group display-align">
                                            <label class="col-12 control-label ">Số áo</label>
                                            <div class="col-lg-9 col-md-9 col-sm-8">
                                                <input class="form-control numberPlayer" name="numberPlayer" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group display-align">
                                            <label class="col-12 control-label">Vị trí </label>
                                            <div class="col-lg-9 col-md-9 col-sm-8">
                                                <select class="form-control text-capitalize player_position" name="position"><option value="" selected="selected">-- Vui lòng chọn --</option><option value="1">thủ môn</option><option value="2">hậu vệ</option><option value="3">tiền vệ</option><option value="4">tiền đạo</option><option value="5">khác</option></select>
                                            </div>
                                        </div>
                                        <div class="form-group display-align">
                                            <label class="col-12 control-label">Vai trò</label>
                                            <div class="col-lg-9 col-md-9 col-sm-8">
                                                <select class="form-control text-capitalize role" name="role"><option value="1">Vận Động Viên</option><option value="2">Ông Bầu</option><option value="3">Lãnh Đội</option><option value="4" selected="selected">HLV Trưởng</option><option value="5">Trợ Lý HLV</option><option value="6">HLV Thủ Môn</option><option value="7">HLV Thể Lực</option><option value="8">Đội Trưởng</option><option value="9">Đội Phó</option></select>
                                            </div>
                                        </div>
                                        <div class="form-group display-align">
                                            <label class="col-12 control-label">Điện Thoại</label>
                                            <div class="col-lg-9 col-md-9 col-sm-8">
                                                <input class="form-control phone" name="phone" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group display-align">
                                            <label class="col-12 control-label">Ngày sinh</label>
                                            <div class="col-lg-9 col-md-9 col-sm-8">
                                                <input class="width200 form-control datepickerBirthday hasDatepicker" title="Ngày sinh" name="birthday" type="text" value="" id="dp1605932411550">
                                            </div>
                                        </div>
                                        <div class="form-group display-align">
                                            <label class="col-12 control-label">Địa chỉ Email</label>
                                            <div class="col-lg-9 col-md-9 col-sm-8">
                                                <input class="form-control" title="Địa chỉ email" name="email" type="email">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12 mt-15 hidden-xs hidden-sm">
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group display-align text-right wrapButtonFooter">
                                        <button class="btn btn-secondary" data-dismiss="modal" type="reset">
                                            Đóng
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>