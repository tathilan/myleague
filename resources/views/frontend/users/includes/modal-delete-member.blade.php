<div class="modal fade" id="deleteMember" tabindex="-1" role="dialog" aria-labelledby="deleteMember" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center">
                    <h2 class="mb-4">Xác nhận</h2>
                    <h5>Bạn có chắc muốn xóa thành viên này khỏi ?</h5>
                </div>

                <div class="text-center mt-4">
                    <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Hủy bỏ</button>
                    <button type="button" class="btn btn-danger ml-2">Đồng ý</button>
                </div>

            </div>
        </div>
    </div>
</div>