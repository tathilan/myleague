<div class="layout-sidenav-nav">
    <nav class="sb-sidenav sb-sidenav-dark">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <a class="nav-link nav-link-user collapsed" href="{{route('user.showManagementClub')}}">
                    <i class="fa fa-credit-card mr-2"></i>
                    Thông tin đội
                </a>

                <a class="nav-link nav-link-character collapsed" href="{{route('user.showManagementClub')}}">
                    <i class="fa fa-cog mr-2"></i>
                    Chỉnh sửa đội
                </a>
                <a class="nav-link nav-link-deposit-and-payment collapsed" href="{{route('user.showMemberInClub')}}">
                    <i class="fa fa-users mr-2"></i>
                    Thành viên đội
                </a>

                <a class="nav-link nav-link-various-setting collapsed" href="{{route('user.showTournamentOfClub')}}">
                    <i class="far fa-futbol mr-2"></i>
                    Các giải đấu
                </a>

                <a class="nav-link nav-link-gacha collapsed" href="{{route('user.showStatistic')}}">
                    <i class="far fa-chart-bar mr-2"></i>
                    Thống kê
                </a>
            </div>
        </div>
    </nav>
</div>