@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="management-club">
            @include('frontend.users.includes.sidebar')
            <div class="c-wrapper c-fixed-components">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="card">
                            <div class="card-header text-center">
                                Thông tin đội

                            </div><!--card-header-->

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        Ten doi
                                    </div>

                                    <div class="col-12 image-club">
                                        <div class="avatar-club">
                                            <img src="{{asset('image/team.png')}}">
                                        </div>
                                    </div>

                                    <div class="frame-register col-12">
                                        <div class="row">
                                            <div class="col-sm-4 text-center">
                                                <label for="exampleFormControlSelect1">Đồng phục 1</label>
                                                <div class="logo-club mb-3">
                                                    <img width="242" height="242" src="http://0.0.0.0:8088/image/shirt.svg">
                                                </div>
                                            </div>
                                            <div class="col-sm-4 text-center">
                                                <label for="exampleFormControlSelect1">Đồng phục 2</label>
                                                <div class="logo-club mb-3">
                                                    <img width="242" height="242" src="http://0.0.0.0:8088/image/shirt.svg">
                                                </div>
                                            </div>
                                            <div class="col-sm-4 text-center">
                                                <label for="exampleFormControlSelect1">Đồng phục 3</label>
                                                <div class="logo-club mb-3">
                                                    <img width="242" height="242" src="http://0.0.0.0:8088/image/shirt.svg">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        <label class="mt-3">KHU VỰC HOẠT ĐỘNG</label>
                                    </div>

                                    <div class="col-12">
                                        <div class="list-info-other">
                                            <div class="item">
                                                <i class="fa fa-user-circle"></i>
                                                Người liên hệ:
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-users"></i>
                                                Trình độ:
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-birthday-cake"></i>
                                                Độ tuổi:
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-transgender"></i>
                                                Giới tính:
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-envelope"></i>
                                                Email:
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-phone"></i>
                                                Số điện thoại:
                                            </div>
                                            <div class="item-final">
                                                <i class="fa fa-info-circle"></i>
                                                Giới thiệu:
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--card-body-->

                        </div><!--card-->
                    </div><!--fade-in-->
                </div><!--container-fluid-->
            </div>
        </div>
    </div>
@endsection
