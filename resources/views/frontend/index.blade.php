@extends('frontend.layouts.app')

@section('content')
    <div class="header-index" style="background-image: url('{{ asset('image/background.jpg')}}');">
        <div id="header-featured" class="col-12">
            <div id="banner-wrapper">
                <div id="banner" class="container">
                    <h2>Hỗ trợ các đơn vị tổ chức giải đấu một cách đơn giản!!!</h2>
                    <p>LEAGUE giúp đơn giản hóa trong quá trình tạo và quản lý giải đấu mà đơn vị của bạn tổ chức.</p>
                    <p>Hãy sử dụng và trải nhiệm ứng dụng này. </p>
                </div>
            </div>
        </div>
    </div>

    <div id="wrapper">
        <div id="featured-wrapper">
            <div id="featured" class="extra2 margin-btm container">
                <div class="main-title">
                    <h2>MY LEAGUE</h2>
                    <span class="byline">Hỗ trợ nhiều hình thức thi đấu!</span>
                </div>

                <div class="type">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-6 ebox1">
                            <span class="fa fa-pagelines"></span>
                            <div class="title">
                                <h2>Đấu loại trực tiếp</h2>
                            </div>
                            <p>Đấu trực tiếp hoặc knockout là giải đấu đội bóng thua ở mỗi trận đấu sẽ bị loại ngay khỏi
                                giải đấu!</p>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-6 ebox2">
                            <span class="fa fa-futbol-o"></span>
                            <div class="title">
                                <h2>Đấu vòng tròn</h2>
                            </div>
                            <p>Đáu vòng tròn là giải đấu mỗi đội bóng sẽ thi đấu vòng tròn với tất cả các đội bóng bóng
                                khác. Tùy chỉnh các điều lệ giải để có thứ hạng rõ ràng!</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="extra2 container">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-6 ebox1">
                        <span class="fa fa-puzzle-piece"></span>
                        <div class="title">
                            <h2>Chia bảng đấu</h2>
                        </div>
                        <p>Chia bảng đấu là giải đấu được chia làm 3 giai đoạn. Giai đoạn một chia các đội thành các bảng
                            đấu. Giai đoạn hai các đội sẽ thi đấu theo hình thức đấu vòng tròn. Giai đoạn 3 là thi đấu loại
                            trực tiếp!</p>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-6">
                        <span class="fa fa-comments-o"></span>
                        <div class="title">
                            <h2>Vòng tròn - Loại trực tiếp</h2>
                        </div>
                        <p>Vòng tròn -Loại trực tiếp là giải đấu tích hợp hai hình thức thi đấu là vòng tròn và loai trực
                            tiếp!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="group-button">
        <a href="#" class="btn btn-success mr-3">Tạo đội</a>
        <a href="#" class="btn btn-success ml-3">Tạo giải</a>
    </div>

    <div class="distribution" style="background: url('{{asset('image/bg-distribution.jpg')}}') no-repeat;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 image-distribution">
                    <img class="w-100" src="{{asset('image/bg-left-distribution.jpg')}}">
                </div>

                <div class="col-md-6 col-md-offset-1 intro">
                    <div class="section-intro">
                        <h2 class="section-title">Điều hành giải</h2>
                        <p>Có 3 giai đoạn để điều hành một giải đấu</p>
                    </div>
                    <div class="section--process__step">
                        <div class="section--process__icon">
                            <span class="step-order">1</span>
                            <span class="fa fa-list-ul text-primary"></span>
                        </div>
                        <h4 class="section--process__title">Tạo Giải</h4>
                        <div class="check-list">
                            <div class="step">Loại trực tiếp</div>
                            <div class="step">Đấu vòng tròn</div>
                            <div class="step">Chia bảng đấu</div>
                            <div class="step">Vòng tròn - loại trực tiếp</div>
                        </div>
                    </div>
                    <div class="section--process__step">
                        <div class="section--process__icon">
                            <span class="step-order">2</span>
                            <span class="fa fa-cogs text-primary"></span>
                        </div>
                        <h4 class="section--process__title">Tùy chỉnh giải đấu</h4>
                        <div class="check-list">
                            <div class="step">Nhập điều lệ, hình và đia điểm</div>
                            <div class="step">Nhập thông tin của đội/cầu thủ</div>
                            <div class="step">Mời người tham gia</div>
                            <div class="step">Lập lịch đấu</div>
                            <div class="step">Tùy chỉnh giai đoạn</div>
                        </div>
                    </div>
                    <div class="section--process__step">
                        <div class="section--process__icon">
                            <span class="step-order">3</span>
                            <span class="fa fa-check-square text-primary"></span>
                        </div>
                        <h4 class="section--process__title">Điều hành giải</h4>
                        <div class="check-list">
                            <div class="step">Kích hoạt</div>
                            <div class="step">Nhập tỷ số</div>
                            <div class="step">Xem thống kê</div>
                            <div class="step">Chia sẻ với bạn bè</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
