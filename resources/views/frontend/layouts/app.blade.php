<!doctype html>
<html lang="vn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>MY LEAGUE</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link href="" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ mix('frontend/css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('frontend/css/vendor.css') }}" rel="stylesheet">
</head>
<body>
@include('frontend.includes.header')
<div class="c-wrapper c-fixed-components">
        <div class="fade-in">
            @yield('content')
        </div><!--fade-in-->
        @include('frontend.includes.footer')
</div><!--c-wrapper-->

@stack('before-scripts')
<script src="{{ mix('frontend/js/vendor.js') }}"></script>
<script src="{{ mix('frontend/js/app.js') }}"></script>
<livewire:scripts/>
@stack('after-scripts')
</body>
</html>
