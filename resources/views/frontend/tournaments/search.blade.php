@extends('frontend.layouts.app')

@section('content')
    <div class="search-tournament mb-3">
        <div class="container-fluid">
            <div class="row form-search">
                    <div class="col-sm-5">
                        <div class="input-group">
                            <input class="form-control" placeholder="Tên giải đấu, tên người quản lý" name="keyword" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>Tỉnh thành</option>
                                        <option>Hà Nội</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>Hình thức</option>
                                        <option>Loại trực tiếp</option>
                                        <option>Đấu vòng tròn</option>
                                        <option>Chia bảng đấu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>Trạng thái</option>
                                        <option>Đăng đăng ký</option>
                                        <option>Hoạt động</option>
                                        <option>Kết thúc</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row result-search-tournament">
                <div class="col-sm-12 col-md-4 mb-3">
                    <div class="tournament">
                        <div class="image-tournament">
                            <img class="w-100" src="{{asset('image/bg_tournament.png')}}">
                            <div class="status">Đang đăng ký</div>
                        </div>
                        <div class="summary-tournament">
                            <div class="avatar-tournament">
                                <a href="#"><img class="" src="{{asset('image/give-tour-compact.png')}}"></a>
                            </div>
                            <div class="name-tournament">GBG OPEN 2021</div>
                            <div class="other-info">Đấu vòng tròn||Bóng đá sân 7||Bắc Giang</div>
                            <div class="number-club">
                                <i class="far fa-futbol"></i>
                                <span class="mr-5">9</span>

                                <i class="far fa-futbol"></i>
                                <span>9/16</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 mb-3">
                    <div class="tournament">
                        <div class="image-tournament">
                            <img class="w-100" src="{{asset('image/bg_tournament.png')}}">
                            <div class="status">Đang đăng ký</div>
                        </div>
                        <div class="summary-tournament">
                            <div class="avatar-tournament">
                                <a href="#"><img class="" src="{{asset('image/give-tour-compact.png')}}"></a>
                            </div>
                            <div class="name-tournament">GBG OPEN 2021</div>
                            <div class="other-info">Đấu vòng tròn||Bóng đá sân 7||Bắc Giang</div>
                            <div class="number-club">
                                <i class="far fa-futbol"></i>
                                <span class="mr-5">9</span>

                                <i class="far fa-futbol"></i>
                                <span>9/16</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 mb-3">
                    <div class="tournament">
                        <div class="image-tournament">
                            <img class="w-100" src="{{asset('image/bg_tournament.png')}}">
                            <div class="status">Đang đăng ký</div>
                        </div>
                        <div class="summary-tournament">
                            <div class="avatar-tournament">
                                <a href="#"><img class="" src="{{asset('image/give-tour-compact.png')}}"></a>
                            </div>
                            <div class="name-tournament">GBG OPEN 2021</div>
                            <div class="other-info">Đấu vòng tròn||Bóng đá sân 7||Bắc Giang</div>
                            <div class="number-club">
                                <i class="far fa-futbol"></i>
                                <span class="mr-5">9</span>

                                <i class="far fa-futbol"></i>
                                <span>9/16</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
