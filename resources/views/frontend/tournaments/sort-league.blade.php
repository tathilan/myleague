@extends('frontend.layouts.app')

@section('content')
    <div class="management-all">
        @include('frontend.tournaments.includes.header')

        <div class="container-fluid">
            <div class="management-club sort-league">
                @include('frontend.tournaments.includes.sidebar')
                <div class="c-wrapper c-fixed-components">
                    <div class="container-fluid">
                        <div class="fade-in">
                            <div class="card">
                                <div class="card-header text-center">
                                    Sắp xếp bảng đấu
                                </div><!--card-header-->

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="mb-0 font-weight-bold" for="selectNumberClub">Số đội vòng knockout<span class="text-red">*</span></label>
                                            <small id="emailHelp" class="form-text text-muted">Số lượng đội bóng vượt qua vòng bảng.</small>
                                            <select class="form-control" id="selectNumberClub">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="dash"></div>
                                        </div>

                                        <div class="col-sm-12">
                                            <label class="mb-0 font-weight-bold" for="selectNumberClub">Giải đấu gồm 2 bảng.</label>
                                            <small class="form-text text-muted">Bạn có thể thay đổi bảng đấu cho đội bằng cách kéo thả hoặc xóa đấu thủ,hay bạn có thể thay đổi số đội đi tiếp ở ở vòng đấu loại trực tiếp.</small>

                                            <div class="list-league">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="title">
                                                            <span class="name">Bảng A</span>
                                                            <span class="text-simple">chọn</span>
                                                            <input type="text" class="form-control w-25 d-inline" id="" placeholder="" value="2">
                                                            <span class="text-simple">di tiep</span>
                                                        </div>

                                                        <div class="content">
                                                            <div class="sortable">
                                                                <span class="summary-text teamName">Đội #6</span>
                                                                <a class="delete-team" id="delete_or_undelete_competitor_B0" data-id="B0" href="javascript:void(0)">
                                                                    <i class="fa fa-trash-alt" tabindex="0"></i>
                                                                </a>
                                                                <span class="move-competitor"><i class="fa fa-arrows-alt"></i></span>
                                                            </div>

                                                            <div class="sortable">
                                                                <span class="summary-text teamName">Đội #6</span>
                                                                <a class="delete-team" id="delete_or_undelete_competitor_B0" data-id="B0" href="javascript:void(0)">
                                                                    <i class="fa fa-trash-alt" tabindex="0"></i>
                                                                </a>
                                                                <span class="move-competitor"><i class="fa fa-arrows-alt"></i></span>
                                                            </div>

                                                            <div class="sortable">
                                                                <span class="summary-text teamName">Đội #6</span>
                                                                <a class="delete-team" id="delete_or_undelete_competitor_B0" data-id="B0" href="javascript:void(0)">
                                                                    <i class="fa fa-trash-alt" tabindex="0"></i>
                                                                </a>
                                                                <span class="move-competitor"><i class="fa fa-arrows-alt"></i></span>
                                                            </div>

                                                            <div class="sortable">
                                                                <span class="summary-text teamName">Đội #6</span>
                                                                <a class="delete-team" id="delete_or_undelete_competitor_B0" data-id="B0" href="javascript:void(0)">
                                                                    <i class="fa fa-trash-alt" tabindex="0"></i>
                                                                </a>
                                                                <span class="move-competitor"><i class="fa fa-arrows-alt"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="title">
                                                            <span class="name">Bảng A</span>
                                                            <span class="text-simple">chọn</span>
                                                            <input type="text" class="form-control w-25 d-inline" id="" placeholder="" value="2">
                                                            <span class="text-simple">di tiep</span>
                                                        </div>

                                                        <div class="content">
                                                            <div class="sortable">
                                                                <span class="summary-text teamName">Đội #6</span>
                                                                <a class="delete-team" id="delete_or_undelete_competitor_B0" data-id="B0" href="javascript:void(0)">
                                                                    <i class="fa fa-trash-alt" tabindex="0"></i>
                                                                </a>
                                                                <span class="move-competitor"><i class="fa fa-arrows-alt"></i></span>
                                                            </div>

                                                            <div class="sortable">
                                                                <span class="summary-text teamName">Đội #6</span>
                                                                <a class="delete-team" id="delete_or_undelete_competitor_B0" data-id="B0" href="javascript:void(0)">
                                                                    <i class="fa fa-trash-alt" tabindex="0"></i>
                                                                </a>
                                                                <span class="move-competitor"><i class="fa fa-arrows-alt"></i></span>
                                                            </div>

                                                            <div class="sortable">
                                                                <span class="summary-text teamName">Đội #6</span>
                                                                <a class="delete-team" id="delete_or_undelete_competitor_B0" data-id="B0" href="javascript:void(0)">
                                                                    <i class="fa fa-trash-alt" tabindex="0"></i>
                                                                </a>
                                                                <span class="move-competitor"><i class="fa fa-arrows-alt"></i></span>
                                                            </div>

                                                            <div class="sortable">
                                                                <span class="summary-text teamName">Đội #6</span>
                                                                <a class="delete-team" id="delete_or_undelete_competitor_B0" data-id="B0" href="javascript:void(0)">
                                                                    <i class="fa fa-trash-alt" tabindex="0"></i>
                                                                </a>
                                                                <span class="move-competitor"><i class="fa fa-arrows-alt"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="dash"></div>
                                        </div>

                                        <div class="col-sm-12 d-flex align-items-center justify-content-center">
                                            <button class="btn btn-success">Lưu</button>
                                        </div>
                                    </div>
                                </div><!--card-body-->

                            </div><!--card-->
                        </div><!--fade-in-->
                    </div><!--container-fluid-->
                </div>
            </div>
        </div>

    </div>
@endsection
