@extends('frontend.layouts.app')

@section('content')
    <div class="management-all">
        @include('frontend.tournaments.includes.header')

        <div class="container-fluid">
            <div class="management-club schedule-tournament">
                @include('frontend.tournaments.includes.sidebar')
                <div class="c-wrapper c-fixed-components">
                    <div class="container-fluid">
                        <div class="fade-in">
                            <div class="card">
                                <div class="card-header text-center">
                                    Quản lý lịch đấu
                                </div><!--card-header-->

                                <div class="card-body">
                                    <div class="list-tab-match">
                                        <ul class="nav nav-tabs d-flex align-items-center justify-content-center" role="tab-list">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Giai đoạn đấu vòng tròn</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Giai đoạn loại trực tiếp</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                                <div id="buttonsTemplate" class="mt-2 mb-2">
                                                    <div class="buttons-container text-center btn-stageRobin">
                                                        <div class="roundButtonsTemplate">
                                                            <div class="flex-item scrollable-box-hidden">
                                                                <div id="roundsBox" class="scrollable-box-for-round">
                                                                    <ul class="nav nav-tabs list round-button-margin" id="myTab" role="tablist">
                                                                        <li class="nav-item list-item list-item--circle">
                                                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">1</a>
                                                                        </li>
                                                                        <li class="nav-item list-item list-item--circle">
                                                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">2</a>
                                                                        </li>
                                                                        <li class="nav-item list-item list-item--circle">
                                                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">3</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                        <div class="content-tab-panner" id="tabs-small-1" role="tabpanel">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class="round-name p10 font-weight-bold ">Vòng 1</div>
                                                                </div>

                                                                <div class="col-sm-12 mb-3">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <h6>Bang A</h6>

                                                                    <div class="list-match-table">
                                                                        <div class="match-list">
                                                                            <div class="match-item">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="match-item mt-3">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <h6>Bang A</h6>

                                                                    <div class="list-match-table">
                                                                        <div class="match-list">
                                                                            <div class="match-item">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="match-item mt-3">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                    <button class="btn btn-success">Lưu</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                        <div class="content-tab-panner" id="tabs-small-2">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class="round-name p10 font-weight-bold ">Vòng 2</div>
                                                                </div>

                                                                <div class="col-sm-12 mb-3">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <h6>Bang A</h6>

                                                                    <div class="list-match-table">
                                                                        <div class="match-list">
                                                                            <div class="match-item">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="match-item mt-3">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <h6>Bang A</h6>

                                                                    <div class="list-match-table">
                                                                        <div class="match-list">
                                                                            <div class="match-item">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="match-item mt-3">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                    <button class="btn btn-success">Lưu</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                                        <div class="content-tab-panner" id="tabs-small-3">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class="round-name p10 font-weight-bold ">Vòng 3</div>
                                                                </div>

                                                                <div class="col-sm-12 mb-3">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <h6>Bang A</h6>

                                                                    <div class="list-match-table">
                                                                        <div class="match-list">
                                                                            <div class="match-item">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="match-item mt-3">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <h6>Bang A</h6>

                                                                    <div class="list-match-table">
                                                                        <div class="match-list">
                                                                            <div class="match-item">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="match-item mt-3">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                    <button class="btn btn-success">Lưu</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                                <div id="buttonsTemplate" class="mt-2 mb-2">
                                                    <div class="buttons-container text-center btn-stageRobin">
                                                        <div class="roundButtonsTemplate">
                                                            <div class="flex-item scrollable-box-hidden">
                                                                <div id="roundsBox" class="scrollable-box-for-round">
                                                                    <ul class="nav nav-tabs list round-button-margin" id="myTab" role="tablist">
                                                                        <li class="nav-item list-item list-item--circle">
                                                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#semifinal" role="tab" aria-controls="home" aria-selected="true">BK</a>
                                                                        </li>
                                                                        <li class="nav-item list-item list-item--circle">
                                                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#final" role="tab" aria-controls="profile" aria-selected="false">CK</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="semifinal" role="tabpanel" aria-labelledby="home-tab">
                                                        <div class="content-tab-panner" id="tabs-small-1" role="tabpanel">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class="round-name p10 font-weight-bold ">Ban ket</div>
                                                                </div>

                                                                <div class="col-sm-12 mb-3">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class="list-match-table">
                                                                        <div class="match-list">
                                                                            <div class="match-item">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="match-item mt-3">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                    <button class="btn btn-success">Lưu</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="final" role="tabpanel" aria-labelledby="profile-tab">
                                                        <div class="content-tab-panner" id="tabs-small-2">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class="round-name p10 font-weight-bold ">Chung ket</div>
                                                                </div>

                                                                <div class="col-sm-12 mb-3">
                                                                    <div class="dash"></div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class="list-match-table">
                                                                        <div class="match-list">
                                                                            <div class="match-item">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-lg-5 inline">
                                                                                        <div class="match-item--teams">
                                                                                            <div class="w-100">
                                                                                                <div class="w-25 match-item--index">
                                                                                                    <span>#1.</span>
                                                                                                </div>
                                                                                                <div class="w-75 no-pad text-center">
                                                                                                    <span class="" title="#4"> Đội #4  </span>
                                                                                                    <span class="">-</span>
                                                                                                    <span class=""title="#3"> Đội #3  </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-3 address-of-match">
                                                                                        <input class="form-control ui-autocomplete-input location" placeholder="Địa điểm" name="match[1][0][location]" type="text">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 date-of-match">
                                                                                        <input class="form-control date hasDatepicker" placeholder="ngày-tháng-năm" name="match[1][0][date]" type="text" id="dp1606035328091">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-lg-2 time-of-match">
                                                                                        <input class="form-control time hasTimepicker" placeholder="giờ:phút" name="match[1][0][time]" type="text" id="tp1606035328102">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                    <button class="btn btn-success">Lưu</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--card-body-->

                            </div><!--card-->
                        </div><!--fade-in-->
                    </div><!--container-fluid-->
                </div>
            </div>
        </div>

    </div>
@endsection
