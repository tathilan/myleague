@extends('frontend.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="management-all group-stage-tournament">
            @include('frontend.tournaments.includes.header')
            <div class="list-tab-group-stage">
                <ul class="nav nav-tabs" role="tab-list">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-2" role="tab">Vòng đấu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Thời gian</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tabs-2" role="tabpanel">
                        <div class="container-fluid">
                            <div class="section-match">
                                <div class="match-box">
                                    <div class="stage-header">
                                        <div class="row">
                                            <div class="col-md-12 d-flex align-items-center justify-content-center">
                                                <div class="title w-25 float-left">
                                                    Vòng
                                                </div>

                                                <div class="list-tab-round w-75">
                                                    <ul class="nav nav-tabs d-flex align-items-center justify-content-end border-bottom-0" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link round-tab active" id="home-tab" data-toggle="tab" href="#round1" role="tab" aria-controls="home" aria-selected="true">Tất</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link round-tab" id="profile-tab" data-toggle="tab" href="#round2" role="tab" aria-controls="profile" aria-selected="false">BK</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link round-tab" id="contact-tab" data-toggle="tab" href="#round3" role="tab" aria-controls="contact" aria-selected="false">CK</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="col-md-12 box-header-second">
                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="round1" role="tabpanel" aria-labelledby="home-tab">
                                                        <div class="round-1">
                                                            <div class="match-template">
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="round2" role="tabpanel" aria-labelledby="profile-tab">
                                                        <div class="round-1">
                                                            <div class="match-template">
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="round3" role="tabpanel" aria-labelledby="contact-tab">
                                                        <div class="round-1">
                                                            <div class="match-template">
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                        <div class="container-fluid">
                            <div class="section-match">
                                <div class="match-box">
                                    <div class="stage-header">
                                        <div class="row">
                                            <div class="col-md-12 box-header-second">
                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="round1" role="tabpanel" aria-labelledby="home-tab">
                                                        <div class="round-1">
                                                            <div class="box-header-first">
                                                                Lịch thi đấu
                                                            </div>
                                                            <div class="match-template">
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="match-box-content mt-3">
                                                                    <div class="row">
                                                                        <div class="col-md-2 item-round d-flex align-items-center justify-content-center">
                                                                            <span class="item-group">#1</span>
                                                                            <span>Vòng 1</span>
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-end">
                                                                            <span class="mr-2">Doi #1</span>
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                        </div>
                                                                        <div class="col-md-2 text-center">
                                                                            Lich thi dau
                                                                        </div>
                                                                        <div class="col-md-4 d-flex align-items-center justify-content-start">
                                                                            <span class="d-inline-block"><img src="{{asset('image/team.png')}}" style="width: 20px;" alt="team"></span>
                                                                            <span class="ml-2">Doi #1</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
