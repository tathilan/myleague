@extends('frontend.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="management-all group-stage-tournament">
            @include('frontend.tournaments.includes.header')
            <div class="list-tab-group-stage">
                <ul class="nav nav-tabs" role="tab-list">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-2" role="tab">Vòng đấu bảng</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Vòng loại trực tiếp</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tabs-2" role="tabpanel">
                        <div class="container-fluid">
                            <div style="background: #676ec1; color: #fff; font-weight: bold;" class="p-3 mt-3">Bang A</div>
                            <table class="table table-striped mt-3">
                                <thead style="background: #808080;">
                                <tr class="text-white">
                                    <th scope="col">#</th>
                                    <th scope="col">Tên đội</th>
                                    <th scope="col">Số trận</th>
                                    <th scope="col">T-H-B</th>
                                    <th scope="col">Hiệu số</th>
                                    <th scope="col">Đỏ/ vàng</th>
                                    <th scope="col">Điểm</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>
                                        <div class="d-inline-flex">
                                            <span class="mr-1" style="color:#3bbf1a;"><i class="fa fa-edit"></i></span>
                                            <span class="mr-1"><img src="{{asset('image/team.png')}}" style="width: 20px;"></span>
                                            <span class="mr-1"><a href="#">Doi #1</a></span>
                                        </div>
                                    </td>
                                    <td>0</td>
                                    <td>0-0-0</td>
                                    <td>0/0</td>
                                    <td>0/0</td>
                                    <td>0</td>
                                </tr>
                                </tbody>
                            </table>

                            <div style="background: #676ec1; color: #fff; font-weight: bold;" class="p-3 mt-3">Bang A</div>
                            <table class="table table-striped mt-3">
                                <thead style="background: #808080;">
                                <tr class="text-white">
                                    <th scope="col">#</th>
                                    <th scope="col">Tên đội</th>
                                    <th scope="col">Số trận</th>
                                    <th scope="col">T-H-B</th>
                                    <th scope="col">Hiệu số</th>
                                    <th scope="col">Đỏ/ vàng</th>
                                    <th scope="col">Điểm</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>
                                        <div class="d-inline-flex">
                                            <span class="mr-1" style="color:#3bbf1a;"><i class="fa fa-edit"></i></span>
                                            <span class="mr-1"><img src="{{asset('image/team.png')}}" style="width: 20px;"></span>
                                            <span class="mr-1"><a href="#">Doi #1</a></span>
                                        </div>
                                    </td>
                                    <td>0</td>
                                    <td>0-0-0</td>
                                    <td>0/0</td>
                                    <td>0/0</td>
                                    <td>0</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                        <div class="container-fluid">
                            <table class="table table-striped mt-3">
                                <thead style="background: #808080;">
                                <tr class="text-white">
                                    <th scope="col">#</th>
                                    <th scope="col">Tên đội</th>
                                    <th scope="col">Số trận</th>
                                    <th scope="col">T-H-B</th>
                                    <th scope="col">Hiệu số</th>
                                    <th scope="col">Đỏ/ vàng</th>
                                    <th scope="col">Điểm</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>
                                        <div class="d-inline-flex">
                                            <span class="mr-1" style="color:#3bbf1a;"><i class="fa fa-edit"></i></span>
                                            <span class="mr-1"><img src="{{asset('image/team.png')}}" style="width: 20px;"></span>
                                            <span class="mr-1"><a href="#">Doi #1</a></span>
                                        </div>
                                    </td>
                                    <td>0</td>
                                    <td>0-0-0</td>
                                    <td>0/0</td>
                                    <td>0/0</td>
                                    <td>0</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
