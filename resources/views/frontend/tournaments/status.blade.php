@extends('frontend.layouts.app')

@section('content')
    <div class="management-all">
        @include('frontend.tournaments.includes.header')

        <div class="container-fluid">
            <div class="management-club setting-status-tournament">
                @include('frontend.tournaments.includes.sidebar')
                <div class="c-wrapper c-fixed-components">
                    <div class="container-fluid">
                        <div class="fade-in">
                            <div class="card">
                                <div class="card-header text-center">
                                    Trạng thái giải đấu

                                </div><!--card-header-->

                                <div class="card-body">
                                    <div class="status-tournament">
                                        <div class="row">
                                            <div class="col-md-12 d-flex align-items-center justify-content-center">
                                                <div class="col-md-4 float-left">
                                                    <span>Trạng thái hiện tại</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <span class="label">Chưa kích hoạt</span>
                                                </div>
                                            </div>

                                            <div class="col-md-12 mt-3 d-flex align-items-center justify-content-center">
                                                <div class="col-md-4 float-left">
                                                    <span>Tổng số trận </span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div><span class="big-text mr-1">15</span><span>trận</span></div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 mt-3 mb-3 d-flex align-items-center justify-content-center">
                                                <button class="btn btn-success">Kích hoạt</button>
                                            </div>
                                        </div>
                                    </div>

                                </div><!--card-body-->

                            </div><!--card-->
                        </div><!--fade-in-->
                    </div><!--container-fluid-->
                </div>
            </div>
        </div>

    </div>
@endsection
