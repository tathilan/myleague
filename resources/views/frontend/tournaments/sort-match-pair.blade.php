@extends('frontend.layouts.app')

@section('content')
    <div class="management-all">
        @include('frontend.tournaments.includes.header')

        <div class="container-fluid">
            <div class="management-club sort-match-pair">
                @include('frontend.tournaments.includes.sidebar')
                <div class="c-wrapper c-fixed-components">
                    <div class="container-fluid">
                        <div class="fade-in">
                            <div class="card">
                                <div class="card-header text-center">
                                    Sắp xếp đội đấu
                                </div><!--card-header-->

                                <div class="card-body">
                                    <div class="list-tab-match">
                                        <ul class="nav nav-tabs d-flex align-items-center justify-content-center" role="tab-list">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Giai đoạn đấu vòng tròn</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Giai đoạn loại trực tiếp</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                                <div id="buttonsTemplate" class="mt-2 mb-2">
                                                    <div class="buttons-container text-center btn-stageRobin">
                                                        <div class="roundButtonsTemplate">
                                                            <div class="flex-item scrollable-box-hidden">
                                                                <div id="roundsBox" class="scrollable-box-for-round">
                                                                    <ul class="nav nav-tabs list round-button-margin" id="myTab" role="tablist">
                                                                        <li class="nav-item list-item list-item--circle">
                                                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">1</a>
                                                                        </li>
                                                                        <li class="nav-item list-item list-item--circle">
                                                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">2</a>
                                                                        </li>
                                                                        <li class="nav-item list-item list-item--circle">
                                                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">3</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                        <div class="content-tab-panner" id="tabs-small-1" role="tabpanel">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="round-info">
                                                                        <div class="name-league">
                                                                            Bảng A
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="round-info">
                                                                        <div class="name-league">
                                                                            Bảng B
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                    <button class="btn btn-success">Lưu</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                        <div class="content-tab-panner" id="tabs-small-2">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="round-info">
                                                                        <div class="name-league">
                                                                            Bảng A
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="round-info">
                                                                        <div class="name-league">
                                                                            Bảng B
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                    <button class="btn btn-success">Lưu</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                                        <div class="content-tab-panner" id="tabs-small-3">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="round-info">
                                                                        <div class="name-league">
                                                                            Bảng A
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="round-info">
                                                                        <div class="name-league">
                                                                            Bảng B
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="match">
                                                                            <span class="stt">#1</span>
                                                                            <div class="intro-match matchInfo " id="A1">
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3"> Đội #3 </option>
                                                                                        <option value="#4" selected=""> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#4">
                                                                                </div> <div class="text-center versus">-</div>
                                                                                <div class="competitor-container">
                                                                                    <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                        <option value="#1"> Đội #1 </option>
                                                                                        <option value="#2"> Đội #2 </option>
                                                                                        <option value="#3" selected=""> Đội #3 </option>
                                                                                        <option value="#4"> Đội #4 </option>
                                                                                        <option value="#5"> Đội #5 </option>
                                                                                        <option value="#6"> Đội #6 </option>
                                                                                        <option value="#7"> Đội #7 </option>
                                                                                        <option value="#8"> Đội #8 </option>
                                                                                    </select>
                                                                                    <input type="hidden" name="" value="#3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                    <button class="btn btn-success">Lưu</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                                <div class="container-fluid">
                                                    <div class="content-tab-panner mt-3">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="round-info">
                                                                    <div class="name-league">
                                                                        Bán kết
                                                                    </div>
                                                                    <div class="match">
                                                                        <span class="stt">#1</span>
                                                                        <div class="intro-match matchInfo " id="A1">
                                                                            <div class="competitor-container">
                                                                                <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                    <option value="#1"> Đội #1 </option>
                                                                                    <option value="#2"> Đội #2 </option>
                                                                                    <option value="#3"> Đội #3 </option>
                                                                                    <option value="#4" selected=""> Đội #4 </option>
                                                                                    <option value="#5"> Đội #5 </option>
                                                                                    <option value="#6"> Đội #6 </option>
                                                                                    <option value="#7"> Đội #7 </option>
                                                                                    <option value="#8"> Đội #8 </option>
                                                                                </select>
                                                                                <input type="hidden" name="" value="#4">
                                                                            </div> <div class="text-center versus">-</div>
                                                                            <div class="competitor-container">
                                                                                <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                    <option value="#1"> Đội #1 </option>
                                                                                    <option value="#2"> Đội #2 </option>
                                                                                    <option value="#3" selected=""> Đội #3 </option>
                                                                                    <option value="#4"> Đội #4 </option>
                                                                                    <option value="#5"> Đội #5 </option>
                                                                                    <option value="#6"> Đội #6 </option>
                                                                                    <option value="#7"> Đội #7 </option>
                                                                                    <option value="#8"> Đội #8 </option>
                                                                                </select>
                                                                                <input type="hidden" name="" value="#3">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="match">
                                                                        <span class="stt">#2</span>
                                                                        <div class="intro-match matchInfo " id="A1">
                                                                            <div class="competitor-container">
                                                                                <select class="team form-control competitorA" name="" id="matchA_538837">
                                                                                    <option value="#1"> Đội #1 </option>
                                                                                    <option value="#2"> Đội #2 </option>
                                                                                    <option value="#3"> Đội #3 </option>
                                                                                    <option value="#4" selected=""> Đội #4 </option>
                                                                                    <option value="#5"> Đội #5 </option>
                                                                                    <option value="#6"> Đội #6 </option>
                                                                                    <option value="#7"> Đội #7 </option>
                                                                                    <option value="#8"> Đội #8 </option>
                                                                                </select>
                                                                                <input type="hidden" name="" value="#4">
                                                                            </div> <div class="text-center versus">-</div>
                                                                            <div class="competitor-container">
                                                                                <select class="team form-control competitorB " name="" id="matchB_538837">
                                                                                    <option value="#1"> Đội #1 </option>
                                                                                    <option value="#2"> Đội #2 </option>
                                                                                    <option value="#3" selected=""> Đội #3 </option>
                                                                                    <option value="#4"> Đội #4 </option>
                                                                                    <option value="#5"> Đội #5 </option>
                                                                                    <option value="#6"> Đội #6 </option>
                                                                                    <option value="#7"> Đội #7 </option>
                                                                                    <option value="#8"> Đội #8 </option>
                                                                                </select>
                                                                                <input type="hidden" name="" value="#3">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 mt-5 d-flex align-items-center justify-content-center">
                                                                <button class="btn btn-success">Lưu</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--card-body-->

                            </div><!--card-->
                        </div><!--fade-in-->
                    </div><!--container-fluid-->
                </div>
            </div>
        </div>

    </div>
@endsection
