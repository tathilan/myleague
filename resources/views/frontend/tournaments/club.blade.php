@extends('frontend.layouts.app')

@section('content')
    <div class="management-all">
        @include('frontend.tournaments.includes.header')

        <div class="container-fluid">
            <div class="management-club tournament-clubs">
                @include('frontend.tournaments.includes.sidebar')
                <div class="c-wrapper c-fixed-components">
                    <div class="container-fluid">
                        <div class="fade-in">
                            <div class="card">
                                <div class="card-header text-center">
                                    Quản lý đội bóng

                                </div><!--card-header-->

                                <div class="card-body">
                                    <div class="list-club">
                                        <div class="col-sm-12 pl-0">
                                            <div class="header-container">
                                                <span>Giới tính: <span class="text-capitalize">Nữ</span></span>
                                            </div>
                                        </div>
                                        <div class="club-container">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <a href="#" class="float-right">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    <div class="avatar-club">
                                                        <img src="{{asset('image/team.png')}}" alt="avatar">
                                                        <span class="index">1</span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-9">
                                                    <div class="info-container">
                                                        <div class="col-sm-12">
                                                            <input class="form-control" placeholder="Tên đội bóng﹡" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                        <div class="col-sm-12 mt-1">
                                                            <input class="form-control" placeholder="SĐT liên hệ" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                        <div class="col-sm-12 mt-1">
                                                            <input class="form-control" placeholder="Tên người liên hệ" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="club-container">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <a href="#" class="float-right">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    <div class="avatar-club">
                                                        <img src="{{asset('image/team.png')}}" alt="avatar">
                                                        <span class="index">1</span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-9">
                                                    <div class="info-container">
                                                        <div class="col-sm-12">
                                                            <input class="form-control" placeholder="Tên đội bóng﹡" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                        <div class="col-sm-12 mt-1">
                                                            <input class="form-control" placeholder="SĐT liên hệ" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                        <div class="col-sm-12 mt-1">
                                                            <input class="form-control" placeholder="Tên người liên hệ" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="club-container">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <a href="#" class="float-right">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    <div class="avatar-club">
                                                        <img src="{{asset('image/team.png')}}" alt="avatar">
                                                        <span class="index">1</span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-9">
                                                    <div class="info-container">
                                                        <div class="col-sm-12">
                                                            <input class="form-control" placeholder="Tên đội bóng﹡" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                        <div class="col-sm-12 mt-1">
                                                            <input class="form-control" placeholder="SĐT liên hệ" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                        <div class="col-sm-12 mt-1">
                                                            <input class="form-control" placeholder="Tên người liên hệ" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="club-container">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <a href="#" class="float-right">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    <div class="avatar-club">
                                                        <img src="{{asset('image/team.png')}}" alt="avatar">
                                                        <span class="index">1</span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-9">
                                                    <div class="info-container">
                                                        <div class="col-sm-12">
                                                            <input class="form-control" placeholder="Tên đội bóng﹡" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                        <div class="col-sm-12 mt-1">
                                                            <input class="form-control" placeholder="SĐT liên hệ" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                        <div class="col-sm-12 mt-1">
                                                            <input class="form-control" placeholder="Tên người liên hệ" title="Tên đội bóng" name="" type="text">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-12 d-flex align-items-center justify-content-center">
                                            <button class="btn btn-success">Lưu</button>
                                        </div>

                                    </div>

                                </div><!--card-body-->

                            </div><!--card-->
                        </div><!--fade-in-->
                    </div><!--container-fluid-->
                </div>
            </div>
        </div>

    </div>
@endsection
