@extends('frontend.layouts.app')

@section('content')
    <div class="create-tournament mb-3">
        <div class="container-fluid d-flex align-items-center justify-content-center">
            <div class="col-sm-9">
                <div class="card">
                    <div class="card-header">
                        Tạo giải đấu
                    </div>
                    <div class="card-body">
                        <div class="frame-register">
                            <div class="row">
                                <div class="col-sm-5">
                                    <label for="exampleFormControlSelect1">Banner của giải đấu</label>

                                    <div class="avatar border-radius clearfix mb-3">
                                        <img class="icon-banner" src="">
                                    </div>

                                    <div class="upload-file mb-3">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <label class="custom-file-label" for="banner">Banner của giải</label>
                                                <input type="file" class="custom-file-input" id="upload-banner">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <label for="name">Tên giải đấu<span class="text-red">*</span></label>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Tên giải đấu">
                                    </div>
                                    <div class="form-group">
                                        <label for="gender">Giới tính<span class="text-red">*</span></label>
                                        <select class="form-control" id="gender" name="gender">
                                            <option>Vui lòng chọn</option>
                                            <option>Nam</option>
                                            <option>Nữ</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Địa điểm tổ chức<span class="text-red">*</span></label>
                                        <input type="text" class="form-control" id="address" placeholder="Địa điểm tổ chức">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="frame-register">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="exampleFormControlSelect1">Hình thức thi đấu<span class="text-red">*</span></label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-competition" id="form-competition-1">
                                                <span>Loại trực tiếp</span>
                                                <span><img src="{{asset('image/competition-2.png')}}"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-competition" id="form-competition-2">
                                                <span>Đấu vòng tròn</span>
                                                <span><img src="{{asset('image/competition-2.png')}}"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-competition" id="form-competition-3">
                                                <span>Chia bảng đấu</span>
                                                <span><img src="{{asset('image/competition-2.png')}}"></span>
                                            </div>
                                        </div>
                                        </div>
                                </div>

                                <div class="col-sm-12 mt-2">
                                    <div class="form-group">
                                        <label for="number_club">Số đội tham gia [2-30]<span class="text-red">*</span></label>
                                        <input type="number" name="number_club" class="form-control" id="number_club" value="2">
                                    </div>
                                </div>
                                <div class="col-sm-12 alert-tournament mb-2">
                                    <div class="alert-suggestion" style="padding:10px">
                                        Đối với cấu hình này thì số lượng trận đấu của giải là: <b class="nummatch">1</b>
                                        <i class="fa fa-info-circle" tabindex="0"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="frame-register content-form-competition-2" id="content-form-competition-2">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="score_win">Điểm thắng<span class="text-red">*</span></label>
                                                <input type="number" name="score_win" class="form-control" id="score_win" value="3">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="score_draw">Điểm hòa<span class="text-red">*</span></label>
                                                <input type="number" name="score_draw" class="form-control" id="score_draw" value="1">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="score_lose">Điểm thua<span class="text-red">*</span></label>
                                                <input type="number" class="form-control"  name="score_lose" id="score_lose" value="0">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="number_round">Số lượt đá vòng<span class="text-red">*</span></label>
                                                <select class="form-control" id="number_round" name="number_round">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content-form-competition-3" id="content-form-competition-3">
                            <div class="frame-register">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="number_group">Số bảng đấu<span class="text-red">*</span></label>
                                                    <input type="number" class="form-control" name="number_group" id="number_group" value="2">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="number_knockout">Số đội vòng knockout<span class="text-red">*</span></label>
                                                    <input type="number" class="form-control" name="number_knockout" id="number_knockout" value="2">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="score_win">Điểm thắng<span class="text-red">*</span></label>
                                                    <input type="number" name="score_win" class="form-control" id="score_win" value="3">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="score_draw">Điểm hòa<span class="text-red">*</span></label>
                                                    <input type="number" name="score_draw" class="form-control" id="score_draw" value="1">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="score_lose">Điểm thua<span class="text-red">*</span></label>
                                                    <input type="number" class="form-control"  name="score_lose" id="score_lose" value="0">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="number_round">Số lượt đá vòng<span class="text-red">*</span></label>
                                                    <select class="form-control" id="number_round" name="number_round">
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="frame-register border-bottom-0">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="number_player">Số lượng người mỗi đội</label>
                                        <select class="form-control" name="number_player" id="number_player">
                                            <option>5</option>
                                            <option>7</option>
                                            <option>9</option>
                                            <option>11</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 d-flex align-items-center justify-content-center">
                            <button type="submit" class="btn btn-success">Tạo giải</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
