<div class="layout-sidenav-nav">
    <nav class="sb-sidenav sb-sidenav-dark">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <a class="nav-link nav-link-user collapsed" href="{{route('user.showManagementClub')}}">
                    <i class="fa fa-credit-card mr-2"></i>
                    Cấu hình giải đấu
                </a>
                <a class="nav-link nav-link-user collapsed" href="{{route('user.tournaments.status')}}">
                    <i class="fa fa-credit-card mr-2"></i>
                    Trạng thái
                </a>
                <a class="nav-link nav-link-user collapsed" href="{{route('user.showManagementClub')}}">
                    <i class="fa fa-credit-card mr-2"></i>
                    Danh sách đăng ký
                </a>
                <a class="nav-link nav-link-character collapsed" href="{{route('user.tournaments.clubs')}}">
                    <i class="fa fa-cog mr-2"></i>
                    Quản lý đội bóng
                </a>
                <a class="nav-link nav-link-deposit-and-payment collapsed" href="{{route('user.tournaments.showSchedule')}}">
                    <i class="fa fa-users mr-2"></i>
                    Quản lý lịch đấu
                </a>

                <a class="nav-link nav-link-various-setting collapsed" href="{{route('user.tournaments.sortLeague')}}">
                    <i class="far fa-futbol mr-2"></i>
                    Sắp xếp bảng đấu
                </a>

                <a class="nav-link nav-link-gacha collapsed" href="{{route('user.tournaments.sortMatchPair')}}">
                    <i class="far fa-chart-bar mr-2"></i>
                    Sắp xếp cặp đấu
                </a>
            </div>
        </div>
    </nav>
</div>