<div class="info-user mb-5">
    <div class="row">
        <div class="col-4 col-md-2">
            <div class="avatar">
                <img src="{{asset('image/team.png')}}">
            </div>
        </div>
        <div class="col-8 col-md-4">
            <div class="email-user mb-2">
                <i class="fas fa-envelope"></i>
                lanta12a5@gmail.com
            </div>
            <div class="phone-number-user mb-2">
                <i class="fas fa-phone-alt"></i>
                0343900246
            </div>
            <div class="time-activity">
                <i class="fas fa-calendar-alt"></i>
                20:30 T3 HMG
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>

</div>
<div class="container-fluid">
    <div class="list-tab-management">
        <ul class="nav nav-tabs" role="tab-list">
            <li class="nav-item">
                <a class="nav-link" href="#" role="tab">CHUNG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" role="tab">VÒNG BẢNG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" role="tab">VÒNG KNOCKOUT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" role="tab">BẢNG XẾP HẠNG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="#" role="tab">ĐỘI THI ĐẤU</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="{{route('user.tournaments.setting')}}" role="tab">CÀI ĐẶT</a>
            </li>
        </ul>
    </div>
</div>