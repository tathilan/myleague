<?php

namespace Database\Seeders;

use App\Models\User;
use DisableForeignKeys;
use Illuminate\Database\Seeder;
use TruncateTable;

class UserTableSeeder extends Seeder
{
//    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->disableForeignKeys();
//        $this->truncate('users');
        User::create([
            'name' => 'user',
            'email' => 'league@league.com',
            'password' => bcrypt('secret'),
            'email_verified_at' => now(),
//            'active' => true,
            'phone_number' => '123444',
            'avatar' => 'https://www.league.com',
            'link_facebook' => 'https://www.league.com'
        ]);
    }
}
