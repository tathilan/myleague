<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('avatar')->nullable();
            $table->string('level')->comment('0: other, 1: happy, 2: intermediate level: , 3: semipro, 4: pro');
            $table->string('telephone_number');
            $table->boolean('gender')->comment('0: male, 1: female');
            $table->boolean('status')->comment('0: secret, 1: public');
            $table->integer('age')->comment('0: 15-20, 1: 20-25, 2: 25-30');
            $table->string('time_activity')->nullable();
            $table->string('area')->nullable();
            $table->string('uniform')->nullable();
            $table->string('introduce')->nullable();
            $table->string('page_club')->nullable();
            $table->string('people_contact')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
