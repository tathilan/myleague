<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tournament_id');
            $table->integer('round');
            $table->integer('club_A_id');
            $table->integer('club_B_id');
            $table->integer('goal_A');
            $table->integer('goal_B');
            $table->integer('yellow_card_A');
            $table->integer('yellow_card_B');
            $table->integer('red_card_A');
            $table->integer('red_card_B');
            $table->string('stage');
            $table->string('competition_time');
            $table->string('address');
            $table->tinyInteger('status');
            $table->integer('sub_goal_A');
            $table->integer('sub_goal_B');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
