<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id');
            $table->string('name');
            $table->string('avatar');
            $table->string('uniform_number');
            $table->string('uniform_name');
            $table->string('position');
            $table->integer('role');
            $table->string('phone');
            $table->string('birthday');
            $table->tinyInteger('is_player_main');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
