<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('gender');
            $table->string('address');
            $table->string('stadium');
            $table->string('type_tournament');
            $table->integer('number_player');
            $table->integer('number_club');
            $table->integer('max_player');
            $table->integer('number_group');
            $table->integer('number_knockout');
            $table->integer('number_round');
            $table->integer('score_win');
            $table->integer('score_draw');
            $table->integer('score_lose');
            $table->tinyInteger('status');
            $table->string('charter');
            $table->string('introduce');
            $table->string('register_permission');
            $table->string('register_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
}
