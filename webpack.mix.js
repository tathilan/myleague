const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public')
    // .setResourceRoot('../') // Turns assets paths in css relative to css file
    .sass('resources/scss/frontend/app.scss', 'frontend/css/app.css')
    .sass('resources/scss/frontend/vendor.scss', 'frontend/css/vendor.css')
    .js('resources/js/frontend/vendor.js', 'frontend/js/vendor.js')
    .js('resources/js/frontend/app.js', 'frontend/js/app.js')
    // .js('resources/js/backend/vendor.js', 'backend/js/vendor.js')
    // .extract([
    //     'alpinejs',
    //     'jquery',
    //     'bootstrap',
    //     'popper.js',
    //     'axios',
    //     'sweetalert2',
    //     'lodash'
    // ])
    .sourceMaps();

if (mix.inProduction()) {
    mix.version()
        .options({
            // Optimize JS minification process
            terser: {
                cache: true,
                parallel: true,
                sourceMap: true
            }
        });
} else {
    // Uses inline source-maps on development
    mix.webpackConfig({
        // devtool: 'inline-source-map'
        resolve: {
            extensions: ['.js'],
        },
        module: {
            rules: [
                {
                    test: /\.js?$/,
                    exclude: /(node_modules|bower_components)/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: Config.babel()
                        }
                    ]
                }
            ]
        }
    });
}
