<?php
/**
 * Created by PhpStorm.
 * Date: 8/24/2020
 *
 * Holds custom app settings
 */
return [
    'path' => [
        'frontend' => [
            'clubs' => 'clubs'
        ],
    ],
];
