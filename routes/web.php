<?php

use App\Http\Controllers\Frontend\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('index');
//});
Route::group(
    [
        'as' => 'user.',
        'middleware' => ['guest:web'],
    ],
    function () {
        Route::get('/user/login', [LoginController::class, 'showLoginForm'])->name('showLoginForm');
        Route::post('/user/login', [LoginController::class, 'attemptLogin'])->name('login');
    }
);

Route::group(
    [
        'as' => 'user.',
        'middleware' => ['auth:web'],
    ],
    function () {
        Route::get('/clubs/create', 'ClubController@showFormCreate')->name('clubs.showFormCreate');
        Route::post('/clubs/create', 'ClubController@store')->name('clubs.store');
        Route::get('/tournaments/create', 'TournamentController@showFormCreate')->name('tournaments.showFormCreate');
        Route::get('/tournaments/dashboard', 'TournamentController@index')->name('tournaments.index');
        Route::get('/tournaments/setting', 'TournamentController@setting')->name('tournaments.setting');
        Route::get('/tournaments/status', 'TournamentController@showStatusTournament')->name('tournaments.status');
        Route::get('/tournaments/clubs', 'TournamentController@showListClub')->name('tournaments.clubs');
        Route::get('/tournaments/sort-league', 'TournamentController@showLeague')->name('tournaments.sortLeague');
        Route::get('/tournaments/sort-match-pair', 'TournamentController@showMatchPair')->name('tournaments.sortMatchPair');
        Route::get('/tournaments/schedule', 'TournamentController@showSchedule')->name('tournaments.showSchedule');
        Route::get('/tournaments/knock-out', 'TournamentController@showKnockOut')->name('tournaments.showKnockOut');
        Route::get('/tournaments/group-stage', 'TournamentController@showGroupStage')->name('tournaments.showGroupStage');
        Route::get('/tournaments/chart', 'TournamentController@showChart')->name('tournaments.showChart');
        Route::get('/user/logout', [LoginController::class,'logout'])->name('logout-user');
        Route::get('/user/management-club', 'UserController@showManagementClub')->name('showManagementClub');
        Route::get('/user/management', 'UserController@showAllManagement')->name('showAllManagement');
        Route::get('/user/management-member', 'UserController@showMemberInClub')->name('showMemberInClub');
        Route::get('/user/management-tournament', 'UserController@showTournamentOfClub')->name('showTournamentOfClub');
        Route::get('/user/achievement-statistics', 'UserController@showStatistic')->name('showStatistic');
    }
);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/clubs/search', 'ClubController@showFormSearch')->name('clubs.showFormSearch');
Route::get('/tournaments/search', 'TournamentController@showFormSearch')->name('tournaments.showFormSearch');
