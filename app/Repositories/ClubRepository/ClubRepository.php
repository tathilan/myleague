<?php

namespace App\Repositories\ClubRepository;

use App\Models\Club;
use App\Repositories\EloquentRepository;

/**
 * Class ClubRepository
 * @package App\Repositories\ClubRepository
 */
class ClubRepository extends EloquentRepository implements ClubRepositoryInterface
{

    /**
     * @return string
     */
    public function getModel()
    {
        return Club::class;
    }
}
