<?php

namespace App\Repositories;

interface RepositoryInterface
{
    /**
     * Get all
     * @return mixed
     */
    public function getAll();

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * @param $id
     * @return mixed
     */
    public function findWithTrashed($id);

    /**
     * @param $id
     * @return mixed
     */
    public function findOrFail($id);

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * @param array $arrayAttributes
     * @return mixed
     */
    public function createMany(array $arrayAttributes);

    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id, array $attributes);

    /**
     * @param array $conditions
     * @param array $data
     * @return mixed
     */
    public function updateBy(array $conditions, array $data);

    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * Get model.
     * @return string
     */
    public function getModel();

    /**
     * @return mixed
     */
    public function originalModel();

    /**
     * @param $key
     * @param $value
     * @param array $with
     * @return mixed
     */
    public function getFirstBy($key, $value, array $with = []);

    /**
     * @param $key
     * @param $value
     * @param array $with
     * @return mixed
     */
    public function getManyBy($key, $value, array $with = []);

    /**
     * @param array $with
     * @return mixed
     */
    public function getManyWithRelations(array $with = []);

    /**
     * @param array $conditions
     * @param $sortColumn
     * @param array $with
     * @return mixed
     */
    public function getManySortBy(array $conditions, $sortColumn, array $with = []);

    /**
     * @param array $conditions
     * @param array $with
     * @return mixed
     */
    public function getManyByMultiple(array $conditions, array $with = []);

    /**
     * @param $key
     * @param $array
     * @param array $with
     * @return mixed
     */
    public function getManyInArray($key, $array, array $with = []);

    /**
     * @param array $with
     * @return mixed
     */
    public function make(array $with = []);

    /**
     * @param $column
     * @param array $with
     * @return mixed
     */
    public function getMaxValueInColumn($column, array $with = []);

    /**
     * @param $key
     * @param array $values
     * @return mixed
     */
    public function deleteMultiInArray($key, array $values);
}
