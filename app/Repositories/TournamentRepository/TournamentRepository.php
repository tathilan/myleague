<?php

namespace App\Repositories\TournamentRepository;

use App\Models\Tournament;
use App\Repositories\EloquentRepository;

/**
 * Class TournamentRepository
 * @package App\Repositories\TournamentRepository
 */
class TournamentRepository extends EloquentRepository implements TournamentRepositoryInterface
{

    /**
     * @return string
     */
    public function getModel()
    {
        return Tournament::class;
    }
}
