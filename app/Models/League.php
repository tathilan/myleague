<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class League extends Model
{
    use Notifiable,
        SoftDeletes;

    protected $table = 'leagues';

    protected $fillable = [
        'tournament_id',
        'name',
        'number_match',
        'number_club',
        'number_to_knockout',
    ];

    //----- Relationship -----//
}
