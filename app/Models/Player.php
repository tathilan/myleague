<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Player extends Model
{
    use Notifiable,
        SoftDeletes;

    protected $table = 'players';

    protected $fillable = [
        'club_id',
        'name',
        'avatar',
        'uniform_number',
        'uniform_name',
        'position',
        'role',
        'phone',
        'birthday',
        'is_player_main',
    ];

    //----- Relationship -----//
}
