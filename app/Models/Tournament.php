<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Tournament extends Model
{
    use Notifiable,
        SoftDeletes;

    protected $table = 'tournaments';

    protected $fillable = [
        'user_id',
        'name',
        'gender',
        'address',
        'stadium',
        'type_tournament',
        'number_player',
        'number_club',
        'max_player',
        'number_group',
        'number_knockout',
        'number_round',
        'score_win',
        'score_draw',
        'score_lose',
        'status',
        'charter',
        'introduce',
        'register_permission',
        'register_date',
    ];

    //----- Relationship -----//
}
