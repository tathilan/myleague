<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Goal extends Model
{
    use Notifiable,
        SoftDeletes;

    protected $table = 'goals';

    protected $fillable = [
        'match_id',
        'player_id',
        'type_goal',
        'goal_time',
    ];

    //----- Relationship -----//
}
