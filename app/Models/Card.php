<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Card extends Model
{
    use Notifiable,
        SoftDeletes;

    protected $table = 'cards';

    protected $fillable = [
        'match_id',
        'player_id',
        'type_card',
        'card_time',
    ];

    //----- Relationship -----//
}
