<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Match extends Model
{
    use Notifiable,
        SoftDeletes;

    protected $table = 'matches';

    protected $fillable = [
        'tournament_id',
        'round',
        'club_A_id',
        'club_B_id',
        'goal_A',
        'goal_B',
        'yellow_card_A',
        'yellow_card_B',
        'red_card_A',
        'red_card_B',
        'stage',
        'competition_time',
        'address',
        'status',
        'sub_goal_A',
        'sub_goal_B',
    ];

    //----- Relationship -----//
}
