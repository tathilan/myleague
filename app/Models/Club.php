<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Club extends Model
{
    use Notifiable,
        SoftDeletes;

    protected $table = 'clubs';

    protected $fillable = [
        'user_id',
        'name',
        'avatar',
        'level',
        'telephone_number',
        'gender',
        'status',
        'age',
        'time_activity',
        'area',
        'uniform',
        'introduce',
        'page_club',
    ];

    //----- Relationship -----//
}
