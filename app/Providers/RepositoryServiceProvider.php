<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            'App\Repositories\ClubRepository\ClubRepositoryInterface',
            'App\Repositories\ClubRepository\ClubRepository'
        );

        $this->app->bind(
            'App\Repositories\TournamentRepository\TournamentRepositoryInterface',
            'App\Repositories\TournamentRepository\TournamentRepository'
        );
    }
}
