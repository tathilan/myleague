<?php

namespace App\Http\Requests\Frontend\Clubs;

use Illuminate\Foundation\Http\FormRequest;

class CreateClubRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd($this->request->all());
        return [
            'name' => 'required|max:255',
            'level' => 'required',
            'telephone_number' => 'required',
            'gender' => 'required',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên đội của bạn!',
            'name.max' => 'Vui lòng nhập tên đội không quá 255 ký tự!',
            'telephone_number.required' => 'Vui lòng nhập số điện thoại liên hệ!',
            'gender.required' => 'Vui lòng chọn giới tính!',
        ];
    }
}
