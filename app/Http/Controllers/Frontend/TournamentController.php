<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class TournamentController
 * @package App\Http\Controllers
 */
class TournamentController extends Controller
{
    public function showFormCreate()
    {
        return view('frontend.tournaments.create');
    }

    public function showFormSearch()
    {
        return view('frontend.tournaments.search');
    }

    public function index()
    {
        return view('frontend.tournaments.index');
    }

    public function setting()
    {
        return view('frontend.tournaments.setting');
    }

    public function showStatusTournament()
    {
        return view('frontend.tournaments.status');
    }

    public function showListClub()
    {
        return view('frontend.tournaments.club');
    }

    public function showLeague()
    {
        return view('frontend.tournaments.sort-league');
    }

    public function showMatchPair()
    {
        return view('frontend.tournaments.sort-match-pair');
    }

    public function showSchedule()
    {
        return view('frontend.tournaments.schedule');
    }

    public function showGroupStage()
    {
        return view('frontend.tournaments.group-stage');
    }

    public function showKnockOut()
    {
        return view('frontend.tournaments.knockout');
    }

    public function showChart()
    {
        return view('frontend.tournaments.chart');
    }
}
