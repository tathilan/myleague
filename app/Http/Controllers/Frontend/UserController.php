<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    public function showManagementClub()
    {
        return view('frontend.users.management-club');
    }

    public function showAllManagement()
    {
        return view('frontend.users.management-all');
    }

    public function showMemberInClub()
    {
        return view('frontend.users.management-member-in-club');
    }

    public function showTournamentOfClub()
    {
        return view('frontend.users.management-tournament');
    }

    public function showStatistic()
    {
        return view('frontend.users.achievement-statistics');
    }
}
