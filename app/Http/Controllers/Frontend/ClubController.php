<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Clubs\CreateClubRequest;
use App\Repositories\ClubRepository\ClubRepositoryInterface;
use App\Services\ClubService\CreateClubService;
use Illuminate\Http\Request;

/**
 * Class ClubController
 * @package App\Http\Controllers
 */
class ClubController extends Controller
{
    private $clubRepository;

    /**
     * ClubController constructor.
     * @param ClubRepositoryInterface $clubRepository
     */
    public function __construct(
        ClubRepositoryInterface $clubRepository
    )
    {
        $this->clubRepository = $clubRepository;
    }

    public function showFormCreate()
    {
        return view('frontend.clubs.create');
    }

    public function store(Request $createClubRequest, CreateClubService $createClubService)
    {
        $data = $createClubService->handle($createClubRequest->except('_token'));

        $this->clubRepository->create($data);
    }

    public function showFormSearch()
    {
        return view('frontend.clubs.search');
    }
}
