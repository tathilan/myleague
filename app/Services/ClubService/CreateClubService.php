<?php

namespace App\Services\ClubService;

use App\Services\Traits\SaveImage;

class CreateClubService
{
    use SaveImage;

    public function __construct()
    {
    }

    public function handle($data)
    {
        $path = 'backend/' . config('settings.path.frontend.clubs');
        if (! empty($data['avatar'])) {
            $this->deleteImage($data['avatar'], $path);
            $data['avatar'] = $this->storeImage($data['avatar'], $path);
        }

        return $data;
    }
}
