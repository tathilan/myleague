<?php

namespace App\Services\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

trait SaveImage
{
    /**
     * @param $image
     * @param $path
     * @return string
     */
    public function storeImage($image, $path)
    {
//        dd($image);
        $extensionImage = $image->getClientOriginalExtension();
        $imageName = $image->getFilename() . '.' . $extensionImage;
        Storage::disk('images')->put($path . '/' . $imageName, File::get($image));
        return $imageName;
    }

    /**
     * @param $imageName
     * @param $path
     * @return bool
     */
    public function deleteImage($imageName, $path)
    {
        return Storage::disk('images')->delete($path . '/' . $imageName);
    }
}
